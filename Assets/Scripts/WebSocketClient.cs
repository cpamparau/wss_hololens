﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if !UNITY_EDITOR
using System.Threading.Tasks;
using Windows;
#endif

public class WebSocketClient : MonoBehaviour
{
#if !UNITY_EDITOR
    private Windows.Networking.Sockets.MessageWebSocket messageWebSocket;
    private Task connectTask = null;
#endif

    public System.Diagnostics.Stopwatch ceas = new System.Diagnostics.Stopwatch();

    public int contor = 0;

    // Use this for initialization

    void Start()
    {
        Debug.Log("WELCOME 2 THE UNITY");

#if !UNITY_EDITOR
        Debug.Log("WELCOME 2 THE HOLOOOO");
        startSocket();
        try
        {
            //"ws://192.168.10.208:81"
            connectTask = this.messageWebSocket.ConnectAsync(new Uri("ws://192.168.10.208:81")).AsTask();
            String mesaj = "Mesajul " + contor.ToString();
            connectTask.ContinueWith(_ => this.SendMessageUsingMessageWebSocketAsync(mesaj));
            contor++;
        }
        catch (Exception ex)
        {
            Windows.Web.WebErrorStatus webErrorStatus = Windows.Networking.Sockets.WebSocketError.GetStatus(ex.GetBaseException().HResult);
            // Add additional code here to handle exceptions.
            Debug.Log(ex.Message);
        }
#endif
        ceas.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (ceas.ElapsedMilliseconds >= 5000)
        {
            ceas.Stop();
            ceas.Reset();
#if !UNITY_EDITOR
            try
            {
                String mesaj = "Mesajul " + contor.ToString();
                connectTask.ContinueWith(_ => this.SendMessageUsingMessageWebSocketAsync(mesaj));
                contor++;
            }
            catch (Exception ex)
            {
                Windows.Web.WebErrorStatus webErrorStatus = Windows.Networking.Sockets.WebSocketError.GetStatus(ex.GetBaseException().HResult);
                // Add additional code here to handle exceptions.
                Debug.Log(ex.Message);
            }
#endif
            ceas.Start();
        }
    }
#if !UNITY_EDITOR

    void startSocket()
    {

        this.messageWebSocket = new Windows.Networking.Sockets.MessageWebSocket();

        // In this example, we send/receive a string, so we need to set the MessageType to Utf8.
        this.messageWebSocket.Control.MessageType = Windows.Networking.Sockets.SocketMessageType.Utf8;

        this.messageWebSocket.MessageReceived += WebSocket_MessageReceived;
        this.messageWebSocket.Closed += WebSocket_Closed;

        
    }

    private async Task SendMessageUsingMessageWebSocketAsync(string message)
    {
        using (var dataWriter = new Windows.Storage.Streams.DataWriter(this.messageWebSocket.OutputStream))
        {
            dataWriter.WriteString(message);
            await dataWriter.StoreAsync();
            dataWriter.DetachStream();
        }
        Debug.Log("Sending message using MessageWebSocket: " + message);
    }

    private void WebSocket_MessageReceived(Windows.Networking.Sockets.MessageWebSocket sender, Windows.Networking.Sockets.MessageWebSocketMessageReceivedEventArgs args)
    {
        try
        {
            using (Windows.Storage.Streams.DataReader dataReader = args.GetDataReader())
            {
                dataReader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
                string message = dataReader.ReadString(dataReader.UnconsumedBufferLength);
                Debug.Log("Message received from MessageWebSocket: " + message);
                //this.messageWebSocket.Dispose();
            }
        }
        catch (Exception ex)
        {
            Windows.Web.WebErrorStatus webErrorStatus = Windows.Networking.Sockets.WebSocketError.GetStatus(ex.GetBaseException().HResult);
            // Add additional code here to handle exceptions.
        }
    }

    private void WebSocket_Closed(Windows.Networking.Sockets.IWebSocket sender, Windows.Networking.Sockets.WebSocketClosedEventArgs args)
    {
        Debug.Log("WebSocket_Closed; Code: " + args.Code + ", Reason: \"" + args.Reason + "\"");
        // Add additional code here to handle the WebSocket being closed.
    }
#endif

}