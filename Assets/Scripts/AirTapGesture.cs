﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirTapGesture : MonoBehaviour, IInputClickHandler
{
    private bool wasTouched = false;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("You toched me! " + this.gameObject.name + ", countAirTapGestures=" 
            + SingletonClass.Instance.countAirTapgestures.ToString());
        wasTouched = !wasTouched;
        SingletonClass.Instance.countAirTapgestures++;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (wasTouched)
        {
            this.gameObject.transform.Rotate(new Vector3(2, 3, 2));
        }
    }
}
